#include <stdio.h>

void greeting();
void render_field(int, int, int, int);
void move_raket(char);
void count_score();
void print_victory();
int move_ball_x();
int move_ball_y();
int change_direction_x();
int change_direction_y();
int move_rocket_left(char, int);
int move_rocket_right(char, int);
int count_score_player_one();
int count_score_player_two();

int main() {
    int winner = 0;
    char input;
    int ball_x = 40;
    int ball_y = 13;
    int ball_step_x = 1;
    int ball_step_y = 1;
    int rocket_left_pos = 13;
    int rocket_right_pos = 13;
    int p1 = 0;
    int p2 = 0;

    while (!winner) {
        render_field(ball_x, ball_y, rocket_left_pos, rocket_right_pos);
        printf("                                   Ping pong\n");
        printf(
            "             Player One                                 Player "
            "Two\n\n");
        printf("                  %d                                         %d\n\n", p1, p2);
        printf(
            "Controls for the game:\na/z - left player\nk/m - right "
            "player\nq - to quit the game\nPlease make a move to start a game\nPlease enjoy\n");
        scanf("%c", &input);
        rocket_left_pos = move_rocket_left(input, rocket_left_pos);
        rocket_right_pos = move_rocket_right(input, rocket_right_pos);
        ball_step_x = change_direction_x(ball_x, ball_y, ball_step_x, rocket_left_pos, rocket_right_pos);
        ball_step_y = change_direction_y(ball_x, ball_y, ball_step_y, rocket_left_pos, rocket_right_pos);
        ball_x = move_ball_x(ball_x, ball_step_x);
        ball_y = move_ball_y(ball_y, ball_step_y);

        p1 = count_score_player_one(ball_x, p1);
        p2 = count_score_player_two(ball_x, p2);
        winner = (p1 == 21 || p2 == 21) ? 1 : 0;
        if (input == 'q' || input == 'Q') {
            printf("\033[H\033[J");
            break;
        }
    }
    print_victory(p1, p2);
    return 0;
}

void render_field(int ball_x, int ball_y, int rocket_left_pos, int rocket_right_pos) {
    char border = '|';
    char top_bot_border = '-';
    char space = ' ';
    char left_raket = '[';
    char right_raket = ']';
    char ball = '*';
    int width = 80;
    int height = 25;

    printf("\033[H\033[J");
    for (int i = 0; i <= height; i++) {
        for (int j = 0; j <= width; j++) {
            if ((i == 0) || (i == 25)) {
                printf("%c", top_bot_border);
            } else if ((i == ball_y) && (j == ball_x)) {
                printf("%c", ball);
            } else if ((j == 0) || (j == 40) || (j == 80)) {
                printf("%c", border);
            } else if (j == 76) {
                if (i == rocket_right_pos - 1 || i == rocket_right_pos || i == rocket_right_pos + 1) {
                    printf("%c", right_raket);
                } else {
                    printf("%c", space);
                }
            } else if (j == 4) {
                if (i == rocket_left_pos - 1 || i == rocket_left_pos || i == rocket_left_pos + 1) {
                    printf("%c", left_raket);
                } else {
                    printf("%c", space);
                }
            } else {
                printf("%c", space);
            }
        }
        printf("\n");
    }
}

int change_direction_x(int ball_x, int ball_y, int ball_step_x, int rocket_left_pos, int rocket_right_pos) {
    if (ball_x == 79 || ball_x == 1) {
        ball_step_x = -ball_step_x;
    }
    if ((ball_x == 5) &&
        (ball_y == rocket_left_pos || ball_y == rocket_left_pos + 1 || ball_y == rocket_left_pos - 1)) {
        ball_step_x = 1;
    }
    if ((ball_x == 75) &&
        (ball_y == rocket_right_pos || ball_y == rocket_right_pos + 1 || ball_y == rocket_right_pos - 1)) {
        ball_step_x = -1;
    }
    return ball_step_x;
}

int change_direction_y(int ball_x, int ball_y, int ball_step_y, int rocket_left_pos, int rocket_right_pos) {
    if (ball_y == 24 || ball_y == 1) {
        ball_step_y = -ball_step_y;
    }
    if (ball_x == 5) {
        if (ball_step_y == 1) {
            if (ball_y == rocket_left_pos) {
                ball_step_y = 0;
            }
            if (ball_y == rocket_left_pos + 1) {
                ball_step_y = -ball_step_y;
            }
        }
        if (ball_step_y == 0) {
            if (ball_y == rocket_left_pos - 1) {
                ball_step_y--;
            }
            if (ball_y == rocket_left_pos + 1) {
                ball_step_y++;
            }
        }
        if (ball_step_y == 1) {
            if (ball_y == rocket_left_pos - 1) {
                ball_step_y = -ball_step_y;
            }
            if (ball_step_y == rocket_left_pos) {
                ball_step_y = 0;
            }
        }
    }
    if (ball_x == 75) {
        if (ball_step_y == -1) {
            if (ball_y == rocket_right_pos) {
                ball_step_y = 0;
            }
            if (ball_y == rocket_right_pos - 1) {
                ball_step_y = -ball_step_y;
            }
        }
        if (ball_step_y == 0) {
            if (ball_y == rocket_right_pos - 1) {
                ball_step_y = -1;
            }
            if (ball_y == rocket_right_pos + 1) {
                ball_step_y = 1;
            }
        }
        if (ball_step_y == -1) {
            if (ball_y == rocket_right_pos) {
                ball_step_y = 0;
            }
            if (ball_y == rocket_right_pos + 1) {
                ball_step_y = -ball_step_y;
            }
        }
    }
    return ball_step_y;
}

int move_ball_x(int ball_x, int ball_step_x) {
    ball_x += ball_step_x;
    return ball_x;
}

int move_ball_y(int ball_y, int ball_step_y) {
    ball_y += ball_step_y;
    return ball_y;
}

int move_rocket_left(char character, int rocket_left_pos) {
    if ((character == 'z' || character == 'Z')) rocket_left_pos++;
    if ((character == 'a' || character == 'A')) rocket_left_pos--;
    if (rocket_left_pos == 1) {
        rocket_left_pos += 1;
    } else if (rocket_left_pos == 24) {
        rocket_left_pos -= 1;
    }
    return rocket_left_pos;
}

int move_rocket_right(char character, int rocket_right_pos) {
    if ((character == 'k' || character == 'K')) rocket_right_pos--;
    if ((character == 'm' || character == 'M')) rocket_right_pos++;
    if (rocket_right_pos == 1) {
        rocket_right_pos += 1;
    } else if (rocket_right_pos == 24) {
        rocket_right_pos -= 1;
    }
    return rocket_right_pos;
}

int count_score_player_one(int ball_x, int p1) {
    if (ball_x == 79) {
        p1++;
    }
    return p1;
}

int count_score_player_two(int ball_x, int p2) {
    if (ball_x == 1) {
        p2++;
    }
    return p2;
}

void print_victory(int p1, int p2) {
    printf("\033[H\033[J");
    if (p1 == 21) {
        printf("Left player won!");
    } else if (p2 == 21) {
        printf("Right player won!");
    }
}
